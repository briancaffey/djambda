"""
This code was adapted from Zappa
"""


from werkzeug.wrappers import Response
from werkzeug.wsgi import ClosingIterator

import django
from django.core.wsgi import get_wsgi_application
from django.core.handlers.wsgi import WSGIHandler

import base64
import os
import datetime
import sys
import json
import logging
import importlib
import collections


logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.INFO)


# add the Lambda root path into the sys.path
# sys.path.append('/var/task')

from utils.wsgi import create_wsgi_request

from django.utils.version import get_version

VERSION = (3, 2, 0, 'alpha', 0)

__version__ = get_version(VERSION)


def django_setup(set_prefix=True):
    """
    Configure the settings (this happens as a side effect of accessing the
    first setting), configure logging and populate the app registry.
    Set the thread-local urlresolvers script prefix if `set_prefix` is True.
    """

    logger.info("import apps")
    from django.apps import apps

    logger.info("import settings")
    from django.conf import settings

    logger.info("urls")
    from django.urls import set_script_prefix

    logger.info("logging")
    from django.utils.log import configure_logging

    logger.info("get_wsgi_application")
    configure_logging(settings.LOGGING_CONFIG, settings.LOGGING)
    if set_prefix:
        set_script_prefix(
            '/'
            if settings.FORCE_SCRIPT_NAME is None
            else settings.FORCE_SCRIPT_NAME
        )
    logger.info("apps.populate")
    logger.info(settings.INSTALLED_APPS)
    logger.info(settings)
    try:
        logger.info("populating apps")
        apps.populate(settings.INSTALLED_APPS)
    except Exception as e:
        logger.info("could not populate apps")
        logger.info(e)


def get_django_wsgi(settings_module):
    logger.info("from django.core.wsgi import get_wsgi_application")

    logger.info("set DJANGO_SETTINGS_MODULE environment variable")
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings_module)

    logger.info("get_wsgi_application")
    django_setup(set_prefix=False)
    return WSGIHandler()


def all_casings(input_string):
    """
    Permute all casings of a given string.

    A pretty algorithm, via @Amber
    http://stackoverflow.com/questions/6792803/finding-all-possible-case-permutations-in-python
    """
    if not input_string:
        yield ""
    else:
        first = input_string[:1]
        if first.lower() == first.upper():
            for sub_casing in all_casings(input_string[1:]):
                yield first + sub_casing
        else:
            for sub_casing in all_casings(input_string[1:]):
                yield first.lower() + sub_casing
                yield first.upper() + sub_casing


def merge_headers(event):
    """
    Merge the values of headers and multiValueHeaders into a single dict.
    Opens up support for multivalue headers via API Gateway and ALB.
    See: https://github.com/Miserlou/Zappa/pull/1756
    """
    headers = event.get('headers') or {}
    multi_headers = (event.get('multiValueHeaders') or {}).copy()
    for h in set(headers.keys()):
        if h not in multi_headers:
            multi_headers[h] = [headers[h]]
    for h in multi_headers.keys():
        multi_headers[h] = ', '.join(multi_headers[h])
    return multi_headers


# https://github.com/Miserlou/Zappa/issues/1188
def titlecase_keys(d):
    """
    Takes a dict with keys of type str and returns a new dict with all keys titlecased.
    """
    return {k.title(): v for k, v in d.items()}


class ZappaWSGIMiddleware:
    """
    Middleware functions necessary for a Zappa deployment.

    Most hacks have now been remove except for Set-Cookie permutation.
    """

    def __init__(self, application):
        self.application = application

    def __call__(self, environ, start_response):
        """
        We must case-mangle the Set-Cookie header name or AWS will use only a
        single one of these headers.
        """

        def encode_response(status, headers, exc_info=None):
            """
            This makes the 'set-cookie' headers name lowercase,
            all the non-cookie headers should be sent unharmed.
            Related: https://github.com/Miserlou/Zappa/issues/1965
            """

            new_headers = [
                header
                for header in headers
                if (
                    (type(header[0]) != str)
                    or (header[0].lower() != 'set-cookie')
                )
            ]
            cookie_headers = [
                (header[0].lower(), header[1])
                for header in headers
                if (
                    (type(header[0]) == str)
                    and (header[0].lower() == "set-cookie")
                )
            ]
            new_headers = new_headers + cookie_headers

            return start_response(status, new_headers, exc_info)

        # Call the application with our modifier
        response = self.application(environ, encode_response)

        # Return the response as a WSGI-safe iterator
        return ClosingIterator(response)


def handler(event, context):

    logger.info("starting!!!!!!!")

    logger.info("get_django_wsgi")
    wsgi_app_function = get_django_wsgi('djambda.settings')

    logger.info("wsgi_app")
    wsgi_app = ZappaWSGIMiddleware(wsgi_app_function)

    # Normal web app flow
    try:
        # Timing
        time_start = datetime.datetime.now()

        # This is a normal HTTP request
        if event.get('httpMethod', None):
            script_name = ''
            is_elb_context = False
            headers = merge_headers(event)
            if event.get('requestContext', None) and event[
                'requestContext'
            ].get('elb', None):
                # Related: https://github.com/Miserlou/Zappa/issues/1715
                # inputs/outputs for lambda loadbalancer
                # https://docs.aws.amazon.com/elasticloadbalancing/latest/application/lambda-functions.html
                is_elb_context = True
                # host is lower-case when forwarded from ELB
                host = headers.get('host')
                # TODO: pathParameters is a first-class citizen in apigateway but not available without
                # some parsing work for ELB (is this parameter used for anything?)
                event['pathParameters'] = ''
            else:
                if headers:
                    host = headers.get('Host')
                else:
                    host = None
                logger.debug('host found: [{}]'.format(host))

                if host:
                    if 'amazonaws.com' in host:
                        logger.debug('amazonaws found in host')
                        # The path provided in th event doesn't include the
                        # stage, so we must tell Flask to include the API
                        # stage in the url it calculates. See https://github.com/Miserlou/Zappa/issues/1014
                        script_name = '/' + 'prod'  # settings.API_STAGE
                else:
                    # This is a test request sent from the AWS console
                    # if settings.DOMAIN:
                    #     # Assume the requests received will be on the specified
                    #     # domain. No special handling is required
                    #     pass
                    # else:
                    # Assume the requests received will be to the
                    # amazonaws.com endpoint, so tell Flask to include the
                    # API stage
                    script_name = '/prod'

            base_path = None  # getattr(settings, 'BASE_PATH', None)

            # Create the environment for WSGI and handle the request
            environ = create_wsgi_request(
                event,
                script_name=script_name,
                base_path=base_path,
                trailing_slash=False,
                binary_support=True,  # settings.BINARY_SUPPORT,
                context_header_mappings={},
            )

            # We are always on https on Lambda, so tell our wsgi app that.
            environ['HTTPS'] = 'on'
            environ['wsgi.url_scheme'] = 'https'
            environ['lambda.context'] = context
            environ['lambda.event'] = event

            # Execute the application
            logger.info("Response.from_app")
            with Response.from_app(wsgi_app, environ) as response:
                # This is the object we're going to return.
                # Pack the WSGI response into our special dictionary.
                zappa_returndict = dict()

                # Issue #1715: ALB support. ALB responses must always include
                # base64 encoding and status description
                if is_elb_context:
                    zappa_returndict.setdefault('isBase64Encoded', False)
                    zappa_returndict.setdefault(
                        'statusDescription', response.status
                    )

                if response.data:
                    if (
                        True  # settings.BINARY_SUPPORT
                        and not response.mimetype.startswith("text/")
                        and response.mimetype != "application/json"
                    ):
                        zappa_returndict['body'] = base64.b64encode(
                            response.data
                        ).decode('utf-8')
                        zappa_returndict["isBase64Encoded"] = True
                    else:
                        zappa_returndict['body'] = response.get_data(
                            as_text=True
                        )

                zappa_returndict['statusCode'] = response.status_code
                if 'headers' in event:
                    zappa_returndict['headers'] = {}
                    for key, value in response.headers:
                        zappa_returndict['headers'][key] = value
                if 'multiValueHeaders' in event:
                    zappa_returndict['multiValueHeaders'] = {}
                    for key, value in response.headers:
                        zappa_returndict['multiValueHeaders'][
                            key
                        ] = response.headers.getlist(key)

                # Calculate the total response time,
                # and log it in the Common Log format.
                time_end = datetime.datetime.now()
                delta = time_end - time_start
                response_time_ms = delta.total_seconds() * 1000
                response.content = response.data
                # common_log(environ, response, response_time=response_time_ms)

                return zappa_returndict
    except Exception as e:  # pragma: no cover
        # Print statements are visible in the logs either way
        print(e)
        exc_info = sys.exc_info()
        message = (
            'An uncaught exception happened while servicing this request. '
            'You can investigate this with the `zappa tail` command.'
        )

        message = 'Failed to import module: {}'.format(message)

        # Call exception handler for unhandled exceptions
        # exception_handler = self.settings.EXCEPTION_HANDLER
        # self._process_exception(
        #     exception_handler=exception_handler,
        #     event=event,
        #     context=context,
        #     exception=e,
        # )

        # Return this unspecified exception as a 500, using template that API Gateway expects.
        content = collections.OrderedDict()
        content['statusCode'] = 500
        body = {'message': message}
        # if settings.DEBUG:  # only include traceback if debug is on.
        #     body['traceback'] = traceback.format_exception(
        #         *exc_info
        #     )  # traceback as a list for readability.
        content['body'] = json.dumps(str(body), sort_keys=True, indent=4)
        return content

