import os

from aws_cdk import (
    core,
    aws_ec2 as ec2,
    aws_lambda as _lambda,
    aws_apigateway as apigw,
    aws_route53 as route53,
    aws_route53_targets as targets,
    aws_certificatemanager as acm,
)

from hosted_zone import HostedZone

# from cert import SiteCertificate
from vpc import VpcStack
from cloudfront import CloudFrontStack
from static_site_bucket import StaticSiteStack
from backend_assets import BackendAssetsStack


class AppStack(core.Stack):
    def __init__(
        self,
        scope: core.Construct,
        id: str,
        environment_name: str,
        base_domain_name: str,
        full_domain_name: str,
        base_app_name: str,
        full_app_name: str,
        **kwargs,
    ) -> None:
        super().__init__(scope, id, **kwargs)

        # The code that defines your stack goes here

        self.environment_name = environment_name
        self.base_domain_name = base_domain_name
        self.full_domain_name = full_domain_name
        self.base_app_name = base_app_name
        self.full_app_name = full_app_name

        self.hosted_zone = HostedZone(self, "HostedZone").hosted_zone

        # self.certificate = SiteCertificate(self, "SiteCert")

        # self.certificate = acm.Certificate.from_certificate_arn(
        #     self, "SiteCert", os.environ.get("CERT_ARN", "")
        # )

        self.vpc_stack = VpcStack(self, "VpcStack")
        # self.vpc = self.vpc_stack.vpc

        # self.static_site_stack = StaticSiteStack(self, "StaticSiteStack")
        # self.static_site_bucket = self.static_site_stack.static_site_bucket

        self.backend_assets = BackendAssetsStack(self, "BackendAssetsStack")
        self.backend_assets_bucket = self.backend_assets.assets_bucket

        # self.cloudfront = CloudFrontStack(self, "CloudFrontStack")

        self.backend_assets_bucket.grant_read_write(
            self.vpc_stack.djambda_lambda
        )
