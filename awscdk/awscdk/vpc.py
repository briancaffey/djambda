import json
import os

from aws_cdk import (
    core,
    aws_ec2 as ec2,
    aws_lambda as _lambda,
    aws_apigateway as apigw,
    aws_route53 as route53,
    aws_route53_targets as targets,
    aws_cloudformation as cloudformation,
    aws_certificatemanager as acm,
    aws_rds as rds,
)


class VpcStack(cloudformation.NestedStack):
    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        self.vpc = ec2.Vpc(
            self,
            "Vpc",
            max_azs=2,
            cidr="10.0.0.0/16",
            nat_gateways=0,
            subnet_configuration=[
                ec2.SubnetConfiguration(
                    subnet_type=ec2.SubnetType.PUBLIC,
                    name="Public",
                    cidr_mask=24,
                ),
                ec2.SubnetConfiguration(
                    subnet_type=ec2.SubnetType.ISOLATED,
                    name="Isolated",
                    cidr_mask=24,
                ),
            ],
        )

        self.vpc.add_gateway_endpoint(
            "S3Gateway", service=ec2.GatewayVpcEndpointAwsService('s3')
        )

        self.lambda_security_group = ec2.SecurityGroup(
            self, "LambdaSecurityGroup", vpc=self.vpc
        )

        self.db_security_group = ec2.CfnSecurityGroup(
            self,
            "DBSecurityGroup",
            vpc_id=self.vpc.vpc_id,
            group_description="DBSecurityGroup",
            security_group_ingress=[
                ec2.CfnSecurityGroup.IngressProperty(
                    ip_protocol="tcp",
                    to_port=5432,
                    from_port=5432,
                    source_security_group_id=self.lambda_security_group.security_group_id,
                )
            ],
        )

        self.db_subnet_group = rds.CfnDBSubnetGroup(
            self,
            "CfnDBSubnetGroup",
            subnet_ids=self.vpc.select_subnets(
                subnet_type=ec2.SubnetType.ISOLATED
            ).subnet_ids,
            db_subnet_group_description=f"{scope.full_app_name}-db-subnet-group",
        )

        self.db_config = {
            "engine_mode": "serverless",
            "engine": "aurora-postgresql",
            "engine_version": "10.7",
            # "port": 5432,
            "enable_http_endpoint": True,
            "master_username": "postgres",
            "master_user_password": os.environ.get(
                "DB_PASSWORD", "db-password"
            ),
            "vpc_security_group_ids": [
                self.db_security_group.get_att("GroupId").to_string()
            ],
            "db_subnet_group_name": self.db_subnet_group.ref,
        }

        self.rds_cluster = rds.CfnDBCluster(
            self, "DBCluster", **self.db_config
        )

        self.env_vars = {
            "POSTGRES_SERVICE_HOST": self.rds_cluster.get_att(
                "Endpoint.Address"
            ).to_string(),
            "POSTGRES_PASSWORD": os.environ.get("DB_PASSWORD", "db-password"),
            "AWS_STORAGE_BUCKET_NAME": f"{scope.full_app_name}-assets",
            "DEBUG": "",
            "DJANGO_SUPERUSER_PASSWORD": os.environ.get(
                "DJANGO_SUPERUSER_PASSWORD", "Mypassword1!"
            ),
            "DJANGO_SUPERUSER_USERNAME": os.environ.get(
                "DJANGO_SUPERUSER_USERNAME", "admin"
            ),
        }

        self.djambda_layer = _lambda.LayerVersion(
            self,
            "DjambdaLayer",
            code=_lambda.AssetCode("./layers/django"),
            compatible_runtimes=[_lambda.Runtime.PYTHON_3_8,],
        )

        self.djambda_lambda = _lambda.Function(
            self,
            "DjambdaLambda",
            runtime=_lambda.Runtime.PYTHON_3_8,
            code=_lambda.AssetCode('./django'),
            function_name=f"{scope.full_app_name}-djambda-lambda",
            handler="djambda.awsgi.lambda_handler",
            layers=[self.djambda_layer],
            timeout=core.Duration.seconds(60),
            vpc=self.vpc,
            vpc_subnets=ec2.SubnetSelection(subnets=self.vpc.isolated_subnets),
            environment={**self.env_vars},
            security_groups=[self.lambda_security_group],
        )

        # Use raw override because Lambda's can't be placed in
        # public subnets using CDK: https://github.com/aws/aws-cdk/issues/8935
        self.djambda_lambda.node.default_child.add_override(
            "Properties.VpcConfig.SubnetIds",
            [subnet.subnet_id for subnet in self.vpc.public_subnets],
        )

        self.apigw = apigw.LambdaRestApi(
            self, 'DjambdaEndpoint', handler=self.djambda_lambda,
        )

        # self.apigw_cert = acm.Certificate(
        #     self,
        #     "ApiGatewayCertificate",
        #     domain_name=f"{scope.full_domain_name}",
        #     validation_method=acm.ValidationMethod.DNS,
        # )

        # Add a custom domain name to API Gateway
        # https://docs.aws.amazon.com/cdk/api/latest/python/aws_cdk.aws_apigateway/LambdaRestApi.html#aws_cdk.aws_apigateway.LambdaRestApi.add_domain_name
        # self.apigw.add_domain_name(
        #     "ApiGwCustomDomain",
        #     certificate=self.apigw_cert,
        #     domain_name=scope.full_domain_name,
        # )
